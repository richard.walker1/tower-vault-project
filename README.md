# tower-vault-project

Ansible Tower Project repository for testing HashiCorp Vault integration. 

See https://dwpdigital.atlassian.net/wiki/spaces/~richard.walker/pages/132438556813/Ansible+Tower+Local+Development#Retrieve-an-individual-secret 